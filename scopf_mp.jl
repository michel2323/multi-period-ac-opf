include("opfdata.jl")

using StructJuMP, JuMP, StructJuMPSolverInterface
# Alternate formulation for the contingency constrainted ac-opf
# 
# 
# 
# 
# 




type SCOPFData
  raw::RawData
  lines_off::Array
  #Float64::gener_ramp #generator ramp limit for contingency (percentage)
end

function scopf_solve(scopfmodel, scopfdata, solver, prof::Bool) 
  
  status = StructJuMPSolverInterface.sj_solve(scopfmodel;solver=solver, with_prof=prof)
  
  # getVarValue(scopfmodel)
  return status
end

block2contingency(pid, k) = (pid >= k+1) ? (pid%(k+1)) : pid    
block2timeperiod(pid,k) = convert(Int32,floor((pid-1)/(k+1))) + 1

#function mpopf_model(raw::RawData, lines_off)
function scopf_model(rawdata, opfdata, opfdem, clist, solver)

    T = opfdem.tsize
    K = length(clist)
    nscen = T*(K+1) # Adding a "no contingency event" 


    lines_off = [opfdata.lines[l] for l in clist]

    opfmodel = StructuredModel(num_scenarios=nscen)

    lines = opfdata.lines; buses = opfdata.buses; generators = opfdata.generators; baseMVA = opfdata.baseMVA;
    nbus  = length(buses); nline= length(lines); ngen  = length(generators);
 

    opfmodel = StructuredModel(num_scenarios=nscen)

    #shortcuts for compactness
    lines = opfdata.lines; buses = opfdata.buses; generators = opfdata.generators; baseMVA = opfdata.baseMVA;
    busIdx = opfdata.BusIdx; FromLines = opfdata.FromLines; ToLines = opfdata.ToLines; BusGeners = opfdata.BusGenerators;
    nbus  = length(buses); nline = length(lines); ngen  = length(generators);
 
    #branch admitances
    YffR,YffI,YttR,YttI,YftR,YftI,YtfR,YtfI,YshR,YshI = computeAdmitances(lines, buses, baseMVA)

    @variable(opfmodel, generators[i].Pmin <= Pg0[i=1:ngen,t=1:T] <= generators[i].Pmax)
    @variable(opfmodel, generators[i].Qmin <= Qg0[i=1:ngen,t=1:T] <= generators[i].Qmax)

#    r=0.0001;
#    @variable(opfmodel, -r*generators[i].Pmax<=Pramp0[i=1:ngen,t=1:T]<=r*generators[i].Pmax)
#    @variable(opfmodel, -r*generators[i].Qmax<=Qramp0[i=1:ngen,t=1:T]<=r*generators[i].Qmax)


    #  DO I ADD THIS ?   
    # @NLconstraint(opfmodel, exP[i=1:ngen], generators[i].Pmin <= Pg[i] + Pramp[i] <= generators[i].Pmax)
    # @NLconstraint(opfmodel, exQ[i=1:ngen], generators[i].Qmin <= Qg[i] + Qramp[i] <= generators[i].Qmax)


    @NLobjective(opfmodel, Min, 0 ) 

    #@NLobjective(opfmodel, Min,  (1/(nscen+1))*sum{ 
    #          generators[i].coeff[generators[i].n-2]*(baseMVA*(Pg0[i,t]))^2 +  
    #          generators[i].coeff[generators[i].n-1]*(baseMVA*(Pg0[i,t])) + 
    #          generators[i].coeff[generators[i].n  ], i=1:ngen,t=1:T})

    #@NLconstraint(opfmodel, exP[i=1:ngen], generators[i].Pmin <= Pg0[i] + Pramp0[i] <= generators[i].Pmax)
    #@NLconstraint(opfmodel, exQ[i=1:ngen], generators[i].Qmin <= Qg0[i] + Qramp0[i] <= generators[i].Qmax)



   # it T>1 
      @NLconstraint(opfmodel, cnstr_ramp[i=1:ngen,t=2:T],  Pg0[i,t] - Pg0[i,t-1] <= opfdata.generators[i].ramp_agc)
   # end  



  for c in getLocalChildrenIds(opfmodel) 

      @show k = block2contingency(c, K)
      @show t = block2timeperiod(c, K)
      @show c
      opfdata_c = opfdata
      if k > 0
          opfdata_c = opf_loaddata(rawdata, lines_off[k]) 
      else
          println("using base case")
      end

      opfmodel_c = StructuredModel(parent=opfmodel,id=c)
    
      #shortcuts for compactness
      lines = opfdata_c.lines; buses = opfdata_c.buses; generators = opfdata_c.generators; baseMVA = opfdata_c.baseMVA;
      busIdx = opfdata_c.BusIdx; FromLines = opfdata_c.FromLines; ToLines = opfdata_c.ToLines; BusGeners = opfdata_c.BusGenerators
      nbus  = length(buses); nline = length(lines); ngen  = length(generators);
  
      YffR,YffI,YttR,YttI,YftR,YftI,YtfR,YtfI,YshR,YshI = computeAdmitances(opfdata_c.lines, opfdata_c.buses, opfdata_c.baseMVA)

    
      r=0.5;
      if k == 0
          r = 0
      end    
      @variable(opfmodel_c, -r*generators[i].Pmax<=Pramp[i=1:ngen]<=r*generators[i].Pmax)
      @variable(opfmodel_c, -r*generators[i].Qmax<=Qramp[i=1:ngen]<=r*generators[i].Qmax)

      @variable(opfmodel_c, generators[i].Pmin <= Pg[i=1:ngen] <= generators[i].Pmax)
      @variable(opfmodel_c, generators[i].Qmin <= Qg[i=1:ngen] <= generators[i].Qmax)
      @variable(opfmodel_c, buses[i].Vmin <= Vm[i=1:nbus] <= buses[i].Vmax)
      @variable(opfmodel_c, Va[1:nbus])


      @NLconstraint(opfmodel_c, _coupling[i=1:ngen], Pg[i] == Pg0[i,t]) 
      @NLconstraint(opfmodel_c, _coupling[i=1:ngen], Qg[i] == Qg0[i,t]) 



      #fix the voltage angle at the reference bus
      if solver == "PipsNlp"                                                                 
	       @NLconstraint(opfmodel_c, _refbus1, Va[opfdata_c.bus_ref] == buses[opfdata_c.bus_ref].Va)
	       @NLconstraint(opfmodel_c, _refbus2, Va[opfdata_c.bus_ref] == buses[opfdata_c.bus_ref].Va)
      elseif solver == "Ipopt"
	       setlowerbound(Va[opfdata_c.bus_ref], buses[opfdata_c.bus_ref].Va)
	       setupperbound(Va[opfdata_c.bus_ref], buses[opfdata_c.bus_ref].Va)
      else   
	       error("Unsupported solver!")
      end 


      if k == 0
          @NLobjective(opfmodel_c, Min, 0*sum{ generators[i].coeff[generators[i].n-2]*(baseMVA*(Pg[i]))^2 
            + generators[i].coeff[generators[i].n-1]*(baseMVA*(Pg[i]))
            + generators[i].coeff[generators[i].n  ], i=1:ngen})
      else  
          @NLobjective(opfmodel_c, Min, 0 ) 
      end              



    # power flow balance
        Pd = opfdem.Pd[:,t]
        Qd = opfdem.Qd[:,t]
    
    # power flow balance
    for b in 1:nbus
      #real part
     @NLconstraint(
        opfmodel_c, 
        ( sum{ YffR[l], l in FromLines[b]} + sum{ YttR[l], l in ToLines[b]} + YshR[b] ) * Vm[b]^2 
        + sum{ Vm[b]*Vm[busIdx[lines[l].to]]  *( YftR[l]*cos(Va[b]-Va[busIdx[lines[l].to]]  ) + YftI[l]*sin(Va[b]-Va[busIdx[lines[l].to]]  )), l in FromLines[b] }  
        + sum{ Vm[b]*Vm[busIdx[lines[l].from]]*( YtfR[l]*cos(Va[b]-Va[busIdx[lines[l].from]]) + YtfI[l]*sin(Va[b]-Va[busIdx[lines[l].from]])), l in ToLines[b]   } 
        - ( sum{baseMVA*(Pg[g] + Pramp[g]), g in BusGeners[b]} - Pd[b] ) / baseMVA      # Sbus part
        ==0)

      #imaginary part
      @NLconstraint(
        opfmodel_c,
        ( sum{-YffI[l], l in FromLines[b]} + sum{-YttI[l], l in ToLines[b]} - YshI[b] ) * Vm[b]^2 
        + sum{ Vm[b]*Vm[busIdx[lines[l].to]]  *(-YftI[l]*cos(Va[b]-Va[busIdx[lines[l].to]]  ) + YftR[l]*sin(Va[b]-Va[busIdx[lines[l].to]]  )), l in FromLines[b] }
        + sum{ Vm[b]*Vm[busIdx[lines[l].from]]*(-YtfI[l]*cos(Va[b]-Va[busIdx[lines[l].from]]) + YtfR[l]*sin(Va[b]-Va[busIdx[lines[l].from]])), l in ToLines[b]   }
        - ( sum{baseMVA*(Qg[g] + Qramp[g]), g in BusGeners[b]} - Qd[b] ) / baseMVA      #Sbus part
        ==0)
    end

    # WHY NLCONSTRAINT in Feng's version ?
    #generator max
    #@NLconstraint(opfmodel_c, ex[i=1:ngen], generators[i].Pmin <= Pg[i] + extra[i] <= generators[i].Pmax)
    @NLconstraint(opfmodel_c, exP[i=1:ngen], generators[i].Pmin <= Pg[i] + Pramp[i] <= generators[i].Pmax)
    @NLconstraint(opfmodel_c, exQ[i=1:ngen], generators[i].Qmin <= Qg[i] + Qramp[i] <= generators[i].Qmax)

    #
    # branch/lines flow limits
    #
    nlinelim=0
    for l in 1:nline
      if lines[l].rateA!=0 && lines[l].rateA<1.0e10
        nlinelim += 1
        flowmax=(lines[l].rateA/baseMVA)^2

        #branch apparent power limits (from bus)
        Yff_abs2=YffR[l]^2+YffI[l]^2; Yft_abs2=YftR[l]^2+YftI[l]^2
        Yre=YffR[l]*YftR[l]+YffI[l]*YftI[l]; Yim=-YffR[l]*YftI[l]+YffI[l]*YftR[l]
        @NLconstraint(
          opfmodel_c,
          Vm[busIdx[lines[l].from]]^2 *
          ( Yff_abs2*Vm[busIdx[lines[l].from]]^2 + Yft_abs2*Vm[busIdx[lines[l].to]]^2 
            + 2*Vm[busIdx[lines[l].from]]*Vm[busIdx[lines[l].to]]*(Yre*cos(Va[busIdx[lines[l].from]]-Va[busIdx[lines[l].to]])-Yim*sin(Va[busIdx[lines[l].from]]-Va[busIdx[lines[l].to]])) 
          ) 
          - flowmax <=0)
  
        #branch apparent power limits (to bus)
        Ytf_abs2=YtfR[l]^2+YtfI[l]^2; Ytt_abs2=YttR[l]^2+YttI[l]^2
        Yre=YtfR[l]*YttR[l]+YtfI[l]*YttI[l]; Yim=-YtfR[l]*YttI[l]+YtfI[l]*YttR[l]
        @NLconstraint(
          opfmodel_c, 
          Vm[busIdx[lines[l].to]]^2 *
          ( Ytf_abs2*Vm[busIdx[lines[l].from]]^2 + Ytt_abs2*Vm[busIdx[lines[l].to]]^2
            + 2*Vm[busIdx[lines[l].from]]*Vm[busIdx[lines[l].to]]*(Yre*cos(Va[busIdx[lines[l].from]]-Va[busIdx[lines[l].to]])-Yim*sin(Va[busIdx[lines[l].from]]-Va[busIdx[lines[l].to]]))
          )
          - flowmax <=0)
      end
    end

    @printf("Contingency %d -> Buses: %d  Lines: %d  Generators: %d\n", c, nbus, nline, ngen)
    println("     lines with limits:  ", nlinelim)
  end
  # ) #end second stage
  @show "acopf_model_sc  - done"
  return opfmodel, nothing
end



function mpopf_init_x(opfmodel,opfdata,demdata)
    T = demdata.tsize
    ngen = length(opfdata.generators)
    nbus = length(opfdata.buses)
    for block_id in getLocalBlocksIds(opfmodel)
        mm = opfmodel
        if  block_id> 0
            mm = getchildren(opfmodel)[block_id]
            _Vm=zeros(nbus)   
            for (i,b) in enumerate(opfdata.buses)
                _Vm[i]=0.5*(b.Vmax+b.Vmin); 
            end  
            _Va = opfdata.buses[opfdata.bus_ref].Va * ones(nbus)
            setvalue(getvariable(mm, :Vm), _Vm)
            setvalue(getvariable(mm, :Va), _Va)
            setvalue(getvariable(mm, :Pramp), zeros(ngen))
            setvalue(getvariable(mm, :Qramp), zeros(ngen))
            _Pg=zeros(ngen); 
            _Qg=zeros(ngen); 
            for (i,g) in enumerate(opfdata.generators)
                _Pg[i]=0.5*(g.Pmax+g.Pmin)
                _Qg[i]=0.5*(g.Qmax+g.Qmin)
            end 
            setvalue(getvariable(mm, :Pg), _Pg)
            setvalue(getvariable(mm, :Qg), _Qg)
        else    
            _Pg0=zeros(ngen,T); 
            _Qg0=zeros(ngen,T); 
            _Pramp0=zeros(ngen,T); 
            _Qramp0=zeros(ngen,T); 
            for (i,g) in enumerate(opfdata.generators), t in 1:T
                _Pg0[i,t]=0.5*(g.Pmax+g.Pmin)
                _Qg0[i,t]=0.5*(g.Qmax+g.Qmin)
            end 
            setvalue(getvariable(opfmodel, :Pg0), _Pg0)  
            setvalue(getvariable(opfmodel, :Qg0), _Qg0)  
            #setvalue(getvariable(opfmodel, :Pramp0), _Pramp0)  
            #setvalue(getvariable(opfmodel, :Qramp0), _Qramp0)  
        end
            
    end
end
function scopf_init_x(scopfmodel,scopfdata)
  raw = scopfdata.raw
  lines_off = scopfdata.lines_off
  for i in getLocalBlocksIds(scopfmodel)
    if(i==0)
      opfdata = opf_loaddata(raw)
      Pg0,Qg0,Vm0,Va0,Pramp0,Qramp0 = scopf_compute_x0(opfdata)
      setvalue(getvariable(scopfmodel, :Pg), Pg0)
      setvalue(getvariable(scopfmodel, :Qg), Qg0)
      setvalue(getvariable(scopfmodel, :Vm), Vm0)
      setvalue(getvariable(scopfmodel, :Va), Va0)
      setvalue(getvariable(scopfmodel, :Pramp), Pramp0)
      setvalue(getvariable(scopfmodel, :Qramp), Qramp0)
      #extra0 = 0.025*Pg0
      #setvalue(getvariable(scopfmodel, :extra), extra0)  
    else
      mm = getchildren(scopfmodel)[i]
      opfdata_c=opf_loaddata(raw,lines_off[i]) 
      Pg0,Qg0,Vm0,Va0,Pramp0,Qramp0  = scopf_compute_x0(opfdata_c)
      setvalue(getvariable(mm, :Vm), Vm0)
      setvalue(getvariable(mm, :Va), Va0)
      setvalue(getvariable(mm, :Pramp), Pramp0)
      setvalue(getvariable(mm, :Qramp), Qramp0)
      #extra0 = 0.025*Pg0
      #setvalue(getvariable(mm, :extra), extra0)
    end
  end
end


# Compute initial point for IPOPT based on the values provided in the case data
function scopf_compute_x0(opfdata)
  Pg=zeros(length(opfdata.generators)); 
  Qg=zeros(length(opfdata.generators)); 

  for (i,g) in enumerate(opfdata.generators)
    Pg[i]=0.5*(g.Pmax+g.Pmin)
    Qg[i]=0.5*(g.Qmax+g.Qmin)
  end 
  Vm=zeros(length(opfdata.buses))   
  for (i,b) in enumerate(opfdata.buses)
    Vm[i]=0.5*(b.Vmax+b.Vmin); 
  end  

  Va = opfdata.buses[opfdata.bus_ref].Va * ones(length(opfdata.buses))
  Pramp = 0.0*Pg
  Qramp = 0.0*Pg


#  for g in opfdata.generators
#    Pg[i]=0.5*(g.Pmax+g.Pmin)
#    Qg[i]=0.5*(g.Qmax+g.Qmin)
#    i=i+1
#  end
#  @assert i-1==length(opfdata.generators)

#  Vm=zeros(length(opfdata.buses)); i=1;
#  for b in opfdata.buses
#    Vm[i]=0.5*(b.Vmax+b.Vmin); 
#    i=i+1
#  end
#  @assert i-1==length(opfdata.buses)

  # set all angles to the angle of the reference bus
 

  return Pg, Qg, Vm, Va, Pramp, Qramp
end

