include("mpopf_debug.jl")
include("opfdata.jl")

SJ =  StructJuMPSolverInterface



function main()
  try ARGS
  catch(UndefVarError)
    ARGS = UTF8String["data/case118","data/case118/sc1", "1"]
  end  
  if length(ARGS) != 3
    println("Usage: julia mopf_main.jl case_name demand_repo nb_time_period")
    return
  end
  case_path = ARGS[1]; shift!(ARGS)
  scenario_path = ARGS[1]; shift!(ARGS)
  nb_time_period = parse(Int,ARGS[1])

  _main(case_path, scenario_path,nb_time_period)
end

# 
function prep_data(opfdata,nb_time_period)
  for i in 1:length(opfdata.generators)
    opfdata.generators[i].ramp_agc = opfdata.generators[i].Pmax * 0.1
  end  

end  

function _main(case_path, scenario_path, nb_time_period)
  rawdata = RawData(case_path, scenario_path)
  opfdata = opf_loaddata(rawdata)
  opfdem = dem_loaddata(rawdata, nb_time_period)
#  @show opfdem

  prep_data(opfdata,nb_time_period)


  opfmodel,soldata = mpopf_model(opfdata, opfdem)
  mpopf_init_x(opfmodel,opfdata,opfdem)
  status = mpopf_solve(opfmodel,soldata)
  return status,opfmodel,opfdem,opfdata,soldata
end

main()
