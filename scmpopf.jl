include("opfdata.jl")

using StructJuMP, JuMP, StructJuMPSolverInterface

#type SCOPFData
#  raw::RawData
#  lines_off::Array
#end

#MAXRAMPUP = ones(54)*3.0


type SolutionData
  Pg0
  Pg
  Qg
  Vm
  Va
  prices
  cnstr_ref
  rampup  
  function SolutionData(opfdata,opfdem, ramp_cnstr)
    ngen = length(opfdata.generators)
    nbus = length(opfdata.buses)
    sol = new()
    sol.Pg = zeros(ngen)
    sol.Qg = zeros(ngen)
    sol.Vm = zeros(nbus)
    sol.Va = zeros(nbus)
    sol.cnstr_ref = Dict(:ramp=>ramp_cnstr)
    sol.prices = Dict()
    sol.rampup  = zeros(ngen,opfdem.tsize)
    sol
  end
end


function mpopf_solve(opfmodel,sol)   
    status = StructJuMPSolverInterface.sj_solve(opfmodel;solver="PipsNlp", with_prof=false)
  
    sol.Pg0 = getvalue(getvariable(opfmodel,:Pg0))
    for c in getLocalChildrenIds(opfmodel)
      mc = SJ.getModel(opfmodel,c)
      sol.Pg = hcat(sol.Pg,getvalue(getvariable(mc,:Pg)))
      sol.Qg = hcat(sol.Qg,getvalue(getvariable(mc,:Qg)))
      sol.Vm = hcat(sol.Vm,getvalue(getvariable(mc,:Vm)))
      sol.Va = hcat(sol.Va,getvalue(getvariable(mc,:Va)))
    end
  #  sol.prices[:ramp] = getdual(sol.cnstr_ref[:ramp])
  return status
end

function post_solve(opfdata,sol)
  ngen,ntime=size(sol.Pg0)
  for i in 1:ngen, t in 2:ntime
    sol.rampup[i,t] =  opfdata.generators[i].ramp_agc - max(0.0,sol.Pg0[i,t] - sol.Pg0[i,t-1]) 
  end  
end


block2contingency(pid, k) = (pid >= k+1) ? (pid%(k+1)) : pid
	
block2timeperiod(pid,k) = convert(Int32,floor((pid-1)/(k+1))) + 1 



function mpopf_model(rawdata, opfdata, opfdem, clist)
    T = opfdem.tsize
    K = length(clist)
    nscen = T*(K+1) # we add a "no contingency event" 


    lines_off = [opfdata.lines[l] for l in clist]


    opfmodel = StructuredModel(num_scenarios=nscen)

    lines = opfdata.lines; buses = opfdata.buses; generators = opfdata.generators; baseMVA = opfdata.baseMVA;
    nbus  = length(buses); nline= length(lines); ngen  = length(generators);
 

    @variable(opfmodel, generators[i].Pmin <= Pg0[i=1:ngen,t=1:T] <= generators[i].Pmax)

    @NLobjective(opfmodel, Min,  (1/(nscen+1))*sum{ generators[i].coeff[generators[i].n-2]*(baseMVA*(Pg0[i,t]))^2 
                   +generators[i].coeff[generators[i].n-1]*(baseMVA*(Pg0[i,t]))
             +generators[i].coeff[generators[i].n  ], i=1:ngen,t=1:T})
  
    @NLconstraint(opfmodel, cnstr_ramp[i=1:ngen,t=2:T],  Pg0[i,t] - Pg0[i,t-1] <= opfdata.generators[i].ramp_agc)


    for c in getLocalChildrenIds(opfmodel) 
        println("------------------")
        @show k = block2contingency(c, K)
        @show t = block2timeperiod(c, K)
        @show c
        @show K

  
    
        opfdata_c = opfdata
        opfmodel_c = StructuredModel(parent=opfmodel,id=c)
        if k > 0
            @show  clist[k]
            opfdata_c = opf_loaddata(rawdata, lines_off[k]) 
        else
            println("using base case")
       end
    

    #shortcuts for compactness
    lines = opfdata_c.lines; buses = opfdata_c.buses; generators = opfdata_c.generators; baseMVA = opfdata_c.baseMVA;
    busIdx = opfdata_c.BusIdx; FromLines = opfdata_c.FromLines; ToLines = opfdata_c.ToLines; BusGeners = opfdata_c.BusGenerators;
    nbus  = length(buses); nline= length(lines); ngen  = length(generators);
    
    #branch admitances
    YffR,YffI,YttR,YttI,YftR,YftI,YtfR,YtfI,YshR,YshI = computeAdmitances(lines, buses, baseMVA)
  
    @variable(opfmodel_c, generators[i].Pmin<=Pg[i=1:ngen]<=generators[i].Pmax)
    @variable(opfmodel_c, generators[i].Qmin<=Qg[i=1:ngen]<=generators[i].Qmax)
    @variable(opfmodel_c, buses[i].Vmin <= Vm[i=1:nbus] <= buses[i].Vmax)
    @variable(opfmodel_c, Va[1:nbus])

    #fix the voltage angle at the reference bus
    setlowerbound(Va[opfdata_c.bus_ref], buses[opfdata_c.bus_ref].Va)
    setupperbound(Va[opfdata_c.bus_ref], buses[opfdata_c.bus_ref].Va)

    @NLobjective(opfmodel_c, Min, (1/(nscen+1))*sum{ generators[i].coeff[generators[i].n-2]*(baseMVA*(Pg[i]))^2 
                         +generators[i].coeff[generators[i].n-1]*(baseMVA*(Pg[i]))
                     +generators[i].coeff[generators[i].n  ], i=1:ngen})

    # Bind active power duplicates to first stage variables
    @NLconstraint(opfmodel_c, _coupling[i=1:ngen], Pg[i] == Pg0[i,t]) 
    
    # power flow balance
    Pd = opfdem.Pd[:,t]
    Qd = opfdem.Qd[:,t]
    for b in 1:nbus
      #real part
     @NLconstraint(
        opfmodel_c, 
        ( sum{ YffR[l], l in FromLines[b]} + sum{ YttR[l], l in ToLines[b]} + YshR[b] ) * Vm[b]^2 
        + sum{ Vm[b]*Vm[busIdx[lines[l].to]]  *( YftR[l]*cos(Va[b]-Va[busIdx[lines[l].to]]  ) + YftI[l]*sin(Va[b]-Va[busIdx[lines[l].to]]  )), l in FromLines[b] }  
        + sum{ Vm[b]*Vm[busIdx[lines[l].from]]*( YtfR[l]*cos(Va[b]-Va[busIdx[lines[l].from]]) + YtfI[l]*sin(Va[b]-Va[busIdx[lines[l].from]])), l in ToLines[b]   } 
        - ( sum{baseMVA*(Pg[g]), g in BusGeners[b]} - Pd[b] ) / baseMVA      # Sbus part
        ==0)

      #imaginary part
      @NLconstraint(
        opfmodel_c,
        ( sum{-YffI[l], l in FromLines[b]} + sum{-YttI[l], l in ToLines[b]} - YshI[b] ) * Vm[b]^2 
        + sum{ Vm[b]*Vm[busIdx[lines[l].to]]  *(-YftI[l]*cos(Va[b]-Va[busIdx[lines[l].to]]  ) + YftR[l]*sin(Va[b]-Va[busIdx[lines[l].to]]  )), l in FromLines[b] }
        + sum{ Vm[b]*Vm[busIdx[lines[l].from]]*(-YtfI[l]*cos(Va[b]-Va[busIdx[lines[l].from]]) + YtfR[l]*sin(Va[b]-Va[busIdx[lines[l].from]])), l in ToLines[b]   }
        - ( sum{baseMVA*(Qg[g] ), g in BusGeners[b]} - Qd[b] ) / baseMVA      #Sbus part
        ==0)
    end

    # branch/lines flow limits
    nlinelim=0
    for l in 1:nline
      if lines[l].rateA!=0 && lines[l].rateA<1.0e10
        nlinelim += 1
        flowmax=(lines[l].rateA/baseMVA)^2

        #branch apparent power limits (from bus)
        Yff_abs2=YffR[l]^2+YffI[l]^2; Yft_abs2=YftR[l]^2+YftI[l]^2
        Yre=YffR[l]*YftR[l]+YffI[l]*YftI[l]; Yim=-YffR[l]*YftI[l]+YffI[l]*YftR[l]
        @NLconstraint(
          opfmodel_c,
          Vm[busIdx[lines[l].from]]^2 *
          ( Yff_abs2*Vm[busIdx[lines[l].from]]^2 + Yft_abs2*Vm[busIdx[lines[l].to]]^2 
            + 2*Vm[busIdx[lines[l].from]]*Vm[busIdx[lines[l].to]]*(Yre*cos(Va[busIdx[lines[l].from]]-Va[busIdx[lines[l].to]])-Yim*sin(Va[busIdx[lines[l].from]]-Va[busIdx[lines[l].to]])) 
          ) 
          - flowmax <=0)
  
        #branch apparent power limits (to bus)
        Ytf_abs2=YtfR[l]^2+YtfI[l]^2; Ytt_abs2=YttR[l]^2+YttI[l]^2
        Yre=YtfR[l]*YttR[l]+YtfI[l]*YttI[l]; Yim=-YtfR[l]*YttI[l]+YtfI[l]*YttR[l]
        @NLconstraint(
          opfmodel_c, 
          Vm[busIdx[lines[l].to]]^2 *
          ( Ytf_abs2*Vm[busIdx[lines[l].from]]^2 + Ytt_abs2*Vm[busIdx[lines[l].to]]^2
            + 2*Vm[busIdx[lines[l].from]]*Vm[busIdx[lines[l].to]]*(Yre*cos(Va[busIdx[lines[l].from]]-Va[busIdx[lines[l].to]])-Yim*sin(Va[busIdx[lines[l].from]]-Va[busIdx[lines[l].to]]))
          )
          - flowmax <=0)
      end
    end
  end
  
  return opfmodel,SolutionData(opfdata,opfdem,cnstr_ramp)
end


function mpopf_init_x(opfmodel,opfdata,demdata)
  T = demdata.tsize
  ngen = length(opfdata.generators)
  nbus = length(opfdata.buses)
  for i in getLocalBlocksIds(opfmodel)
    if(i==0)
        _Pg0=zeros(ngen,T); 
        for (i,g) in enumerate(opfdata.generators), t in 1:T
            _Pg0[i,t]=0.5*(g.Pmax+g.Pmin)
        end 
        setvalue(getvariable(opfmodel, :Pg0), _Pg0)  
    else
        mm = getchildren(opfmodel)[i]
        _Pg=zeros(ngen); 
        _Qg=zeros(ngen); 
        for (i,g) in enumerate(opfdata.generators)
          _Pg[i]=0.5*(g.Pmax+g.Pmin)
          _Qg[i]=0.5*(g.Qmax+g.Qmin)
        end 
        _Vm=zeros(nbus)   
        for (i,b) in enumerate(opfdata.buses)
            _Vm[i]=0.5*(b.Vmax+b.Vmin); 
        end  
        _Va = opfdata.buses[opfdata.bus_ref].Va * ones(nbus)
        setvalue(getvariable(mm, :Vm), _Vm)
        setvalue(getvariable(mm, :Va), _Va)
        setvalue(getvariable(mm, :Pg), _Pg)
        setvalue(getvariable(mm, :Qg), _Qg)
    end
  end
end



