module driver
using params
import MPI
using StructJuMP
export run

include("model.jl")
include("opfdata.jl")
using parallel

# SJ =  StructJuMPSolverInterface


function run()
  params=load_params()
  _main(params)
end

# 
function prep_data(load, opfdata, params)
  for i in 1:length(opfdata.generators)
    opfdata.generators[i].ramp_agc = opfdata.generators[i].Pmax * params.ramp_scaling
  end
  load.Pd *= params.load_scaling
  load.Qd *= params.load_scaling
    
end  

function _main(params)
  case_path, scenario_path, nb_time_period, nb_ctgs, solver = params.case_path, params.scenario_path, params.nb_time_period, params.nb_ctgs, params.solver
  prof = true	
  global debug = false
  global output = false

  prof && (tic() ; tic(); tic() ; tic())
  rawdata = RawData(case_path, scenario_path)
  prof &&  (t_RawData = toq())
	
  network = opf_loaddata(rawdata)
  load = dem_loaddata(rawdata, nb_time_period)
  contingencies = ctgs_loaddata(rawdata, nb_ctgs)

  prep_data(load, network, params)
  prof && (t_prep_data = toq())

  model,soldata = mpopf_model(rawdata, network, load, contingencies, solver, params)
  prof && (t_mpopf_model = toq())
  mpopf_init_x(model,network,load)
  tcomm=getStructure(model).mpiWrapper.comm
  setComm(tcomm)

  prof && (t_user_model_loading = toq())

  prof && (MPI.Barrier(tcomm) ; tic())
  status = mpopf_solve(model, network, solver, prof) 
  prof && (MPI.Barrier(tcomm) ; t_mpopf_solve = toq())
  
  if getAmRoot() && prof
      println("---------------------")
      println("|  Julia Profiling  |")
      println("---------------------")
      println("Load raw data: ", t_RawData, " s.")
      println("Prep. data: ", t_prep_data, " s.")
      println("Create MPOPF model: ", t_mpopf_model, " s.")
      println("Load user model: ", t_user_model_loading, " s.")
      println("Solve: ", t_mpopf_solve, " s.")
  end

 params.verbose && save_sol(model, network,load, params)
 
 
 return model, network, load, params
end

function save_sol(model, network, load, params)
    folder = params.dump_folder
    try
        run(`mkdir $folder`)
    catch err
        nothing
    end
    #println("--------------")
    #for i in 1:length(network.generators)
        #cost = network.generators[i].coeff
        #println("Gen $i coeff = $(cost)")
    #end
   # print(getobjective(model))

    # First stage solution (active power; one row per generator and one column per time period)
    writedlm("$(folder)/first_stage.mat",network.baseMVA*get_first_stage_solution(model))
    # Load at all buses (active power; one row per generator and one column per time period)
    writedlm("$(folder)/load.mat",get_load(load)')
    
    T = load.tsize
    ngen = length(network.generators)
    Pg = getvalue(getvariable(model, :Pg0))
    
    # Ramping (generator id; ramping limit; maximum ramp in the solution)
    data1 = zeros(ngen,3)
    for i in 1:ngen
        data1[i,1] = i
        data1[i,2] =  network.baseMVA*network.generators[i].ramp_agc
        data1[i,3] =  network.baseMVA*maximum([abs(Pg[t+1,i] - Pg[t,i]) for t in 1:T-1])
    end
    writedlm("$(folder)/maxramp.mat",data1)

    # Capacity (generator id; generation limit; maximum generation in the solution)
    data2 = zeros(ngen,3)
    for i in 1:ngen
        data2[i,1] = i
        data2[i,2] =  network.baseMVA*network.generators[i].Pmax
        data2[i,3] =  network.baseMVA*maximum([Pg[t,i] for t in 1:T])
    end
    #writedlm("$(folder)/maxgen.mat",data2)

   
    # comparing the values of the first stage variables (stored above in Pg) and their scenario specific copies (stored below in Pg1) 
    #K = params.nb_ctgs
    #data3 = zeros(ngen*T,4)
    ##for c in getLocalBlocksIds(model)    
    #for c in getLocalChildrenIds(model) 
        #@show k = block2contingency(c, K)
        #@show t = block2timeperiod(c, K)
        #if k == 0 && c != 0
            #@show k,c
            #mm = getchildren(model)[c]
            #@show Pg1 = getvalue(getvariable(mm, :Pg))
            #for i in 1:ngen
                #n = ngen*(t-1)+i
                #data3[n,1] = i
                #data3[n,2] = t
                #data3[n,3] = network.baseMVA*Pg[t,i]
                #data3[n,4] = network.baseMVA*Pg1[i]
            #end    
        #end
        #writedlm("$(folder)/duplication.mat",data3)
    #end
    
    #nothing
end
# MPI.__init__()
# run()
end

