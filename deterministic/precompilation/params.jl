module params
export load_params
type Parameters
    verbose         # Do we create dump dir ?)
    solver          # Ipopt, PipsNlp
    case_path       # Directory with the network files 
    scenario_path   # Directory.prefix of the load scenario files
    nb_time_period  # Number of time period (>0) 
    nb_ctgs         # Number of contingencies (>=0)
    dump_folder     # Output directory for solution dump
    ramp_scaling    # Ramping scaling factor (>0)
    load_scaling    # Load scaling factor (>0)
    r1              # Model parameter  
    r2              # Model parameter
    r3              # Model parameter
end

function load_params()
return Parameters(false,
                    "PipsNlp",
                    pwd() * "/../../data/case118",
                    pwd() * "/../../data/case118/sc2",
                    0,
                    0, 
                    "tmp3", 
                    0.2, 
                    2.0, 
                    0.5, 
                    0.05,
                    0.1)
end
end
