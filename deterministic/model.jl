include("opfdata.jl")
include("parallel.jl")

using StructJuMP, JuMP, StructJuMPSolverInterface
# Alternate formulation for the contingency constrainted ac-opf
# 
# 
# 
# 
# 

debug=true



type SCOPFData
  raw::RawData
  lines_off::Array
  #Float64::gener_ramp #generator ramp limit for contingency (percentage)
end

function mpopf_solve(mpopf_model, mpopf_data, solver, prof::Bool) 
    status = StructJuMPSolverInterface.sj_solve(mpopf_model;solver=solver, with_prof=prof)
    #@show getdual(mpopf_model.ext[:first_stage_constraint])
    return status
end

block2contingency(pid, k) = (pid >= k+1) ? (pid%(k+1)) : pid    
block2timeperiod(pid,k) = convert(Int32,floor((pid-1)/(k+1))) + 1

function mpopf_model(rawdata, opfdata, opfdem, clist, solver, params)

    T = opfdem.tsize
    K = length(clist)
    nscen = T*(K+1) # Adding a "no contingency event" 


    lines_off = [opfdata.lines[l] for l in clist]

    opfmodel = StructuredModel(num_scenarios=nscen)

    lines = opfdata.lines; buses = opfdata.buses; generators = opfdata.generators; baseMVA = opfdata.baseMVA;
    nbus  = length(buses); nline= length(lines); ngen  = length(generators);
 

    opfmodel = StructuredModel(num_scenarios=nscen)

    #shortcuts for compactness
    lines = opfdata.lines; buses = opfdata.buses; generators = opfdata.generators; baseMVA = opfdata.baseMVA;
    busIdx = opfdata.BusIdx; FromLines = opfdata.FromLines; ToLines = opfdata.ToLines; BusGeners = opfdata.BusGenerators;
    nbus  = length(buses); nline = length(lines); ngen  = length(generators);
 
    #branch admitances
    YffR,YffI,YttR,YttI,YftR,YftI,YtfR,YtfI,YshR,YshI = computeAdmitances(lines, buses, baseMVA)

    @variable(opfmodel, generators[i].Pmin <= Pg0[t=1:T,i=1:ngen] <= generators[i].Pmax)
#    @variable(opfmodel, generators[i].Qmin <= Qg0[t=1:T,i=1:ngen] <= generators[i].Qmax)
    @NLobjective(opfmodel, Min, 0 ) 
    @NLconstraint(opfmodel, cnstr_ramp[t=2:T,i=1:ngen],  Pg0[t,i] - Pg0[t-1,i] <= opfdata.generators[i].ramp_agc)
    opfmodel.ext[:first_stage_constraint] = cnstr_ramp 
    for c in getLocalChildrenIds(opfmodel) 

        opfmodel_c = StructuredModel(parent=opfmodel,id=c)

        k = block2contingency(c, K)
        t = block2timeperiod(c, K)
      
        # Network data
        opfdata_c = opfdata

        # Perturbed network data for contingencies (k>0)    
        if k > 0
            opfdata_c = opf_loaddata(rawdata, lines_off[k]) 
        else
            debug && println("using base case")
        end

        # Network data short-hands
        lines = opfdata_c.lines; buses = opfdata_c.buses; generators = opfdata_c.generators; baseMVA = opfdata_c.baseMVA;
        busIdx = opfdata_c.BusIdx; FromLines = opfdata_c.FromLines; ToLines = opfdata_c.ToLines; BusGeners = opfdata_c.BusGenerators
        nbus  = length(buses); nline = length(lines); ngen  = length(generators);
        YffR,YffI,YttR,YttI,YftR,YftI,YtfR,YtfI,YshR,YshI = computeAdmitances(opfdata_c.lines, opfdata_c.buses, opfdata_c.baseMVA)

        # Paraeter r is proportional to the amount of change is allowed in the power injection in the advent of contingencies (k>0)
        r= params.r1 #  0.8;
        if k == 0
            r = params.r2   # 0.0
        end    
        # Corrective measures
        @variable(opfmodel_c, -r*generators[i].Pmax<=Pramp[i=1:ngen]<=r*generators[i].Pmax)
        @variable(opfmodel_c, -r*generators[i].Qmax<=Qramp[i=1:ngen]<=r*generators[i].Qmax)
        # Power inputs (copied from the first stage)
        @variable(opfmodel_c, generators[i].Pmin <= Pg[i=1:ngen] <= generators[i].Pmax)
        @variable(opfmodel_c, generators[i].Qmin <= Qg[i=1:ngen] <= generators[i].Qmax)
        # Voltages (can be anything in each scenario)
        @variable(opfmodel_c, buses[i].Vmin <= Vm[i=1:nbus] <= buses[i].Vmax)
        @variable(opfmodel_c, Va[1:nbus])
        
        @NLconstraint(opfmodel_c, exP[i=1:ngen], generators[i].Pmin <= Pg[i] + Pramp[i] <= generators[i].Pmax)
        @NLconstraint(opfmodel_c, exQ[i=1:ngen], generators[i].Qmin <= Qg[i] + Qramp[i] <= generators[i].Qmax)

        # Bind scenarios with the first stage on active  power
        #@NLconstraint(opfmodel_c, _coupling_1[i=1:ngen], Pg[i] >= Pg0[t,i] - Pg0[t,i] * params.r3 ) 
        #@NLconstraint(opfmodel_c, _coupling_2[i=1:ngen], Pg[i] <= Pg0[t,i] + Pg0[t,i] * params.r3 ) 
        @NLconstraint(opfmodel_c, _coupling_2[i=1:ngen], Pg[i] == Pg0[t,i]) 
        

        # PIPS currently does not deal well with setting bounds on variables after their definition
        if solver == "PipsNlp"                                                                 
	       #@NLconstraint(opfmodel_c, _refbus1, Va[opfdata_c.bus_ref] == buses[opfdata_c.bus_ref].Va)
	       @NLconstraint(opfmodel_c, _refbus2, Va[opfdata_c.bus_ref] == buses[opfdata_c.bus_ref].Va)
        elseif solver == "Ipopt"
	       setlowerbound(Va[opfdata_c.bus_ref], buses[opfdata_c.bus_ref].Va)
	       setupperbound(Va[opfdata_c.bus_ref], buses[opfdata_c.bus_ref].Va)
        else   
	       error("Unsupported solver!")
        end 

        # Only scenarios with $k=0$ contribute to the objective (non contingencies)  
        if k == 0
            @NLobjective(opfmodel_c, Min, sum{ generators[i].coeff[generators[i].n-2]*(baseMVA*(Pg[i]))^2 
            + generators[i].coeff[generators[i].n-1]*(baseMVA*(Pg[i]))
            + generators[i].coeff[generators[i].n  ], i=1:ngen})
        else  
            @NLobjective(opfmodel_c, Min, 0 ) 
        end              
    
        # Power Balance Equations
        # Scenario specific loads
        Pd = opfdem.Pd[:,t] 
        Qd = opfdem.Qd[:,t]

        for b in 1:nbus
            #real part
            @NLconstraint(
                opfmodel_c, 
                ( sum{ YffR[l], l in FromLines[b]} + sum{ YttR[l], l in ToLines[b]} + YshR[b] ) * Vm[b]^2 
                + sum{ Vm[b]*Vm[busIdx[lines[l].to]]  *( YftR[l]*cos(Va[b]-Va[busIdx[lines[l].to]]  ) + YftI[l]*sin(Va[b]-Va[busIdx[lines[l].to]]  )), l in FromLines[b] }  
                + sum{ Vm[b]*Vm[busIdx[lines[l].from]]*( YtfR[l]*cos(Va[b]-Va[busIdx[lines[l].from]]) + YtfI[l]*sin(Va[b]-Va[busIdx[lines[l].from]])), l in ToLines[b]   } 
                - ( sum{baseMVA*(Pg[g] + Pramp[g]), g in BusGeners[b]} - Pd[b] ) / baseMVA      # Sbus part
                ==0)

            #imaginary part
            @NLconstraint(
                opfmodel_c,
                ( sum{-YffI[l], l in FromLines[b]} + sum{-YttI[l], l in ToLines[b]} - YshI[b] ) * Vm[b]^2 
                + sum{ Vm[b]*Vm[busIdx[lines[l].to]]  *(-YftI[l]*cos(Va[b]-Va[busIdx[lines[l].to]]  ) + YftR[l]*sin(Va[b]-Va[busIdx[lines[l].to]]  )), l in FromLines[b] }
                + sum{ Vm[b]*Vm[busIdx[lines[l].from]]*(-YtfI[l]*cos(Va[b]-Va[busIdx[lines[l].from]]) + YtfR[l]*sin(Va[b]-Va[busIdx[lines[l].from]])), l in ToLines[b]   }
                - ( sum{baseMVA*(Qg[g] + Qramp[g]), g in BusGeners[b]} - Qd[b] ) / baseMVA      #Sbus part
                ==0)
        end


        # Line flow limits
        nlinelim=0
        for l in 1:nline
            if lines[l].rateA!=0 && lines[l].rateA<1.0e10
                nlinelim += 1
                flowmax=(lines[l].rateA/baseMVA)^2

                #branch apparent power limits (from bus)
                Yff_abs2=YffR[l]^2+YffI[l]^2; Yft_abs2=YftR[l]^2+YftI[l]^2
                Yre=YffR[l]*YftR[l]+YffI[l]*YftI[l]; Yim=-YffR[l]*YftI[l]+YffI[l]*YftR[l]
                @NLconstraint(
                    opfmodel_c,
                    Vm[busIdx[lines[l].from]]^2 *
                    ( Yff_abs2*Vm[busIdx[lines[l].from]]^2 + Yft_abs2*Vm[busIdx[lines[l].to]]^2 
                    + 2*Vm[busIdx[lines[l].from]]*Vm[busIdx[lines[l].to]]*(Yre*cos(Va[busIdx[lines[l].from]]-Va[busIdx[lines[l].to]])-Yim*sin(Va[busIdx[lines[l].from]]-Va[busIdx[lines[l].to]])) 
                    ) 
                    - flowmax <=0)
  
                #branch apparent power limits (to bus)
                Ytf_abs2=YtfR[l]^2+YtfI[l]^2; Ytt_abs2=YttR[l]^2+YttI[l]^2
                Yre=YtfR[l]*YttR[l]+YtfI[l]*YttI[l]; Yim=-YtfR[l]*YttI[l]+YtfI[l]*YttR[l]
                @NLconstraint(
                    opfmodel_c, 
                    Vm[busIdx[lines[l].to]]^2 *
                    ( Ytf_abs2*Vm[busIdx[lines[l].from]]^2 + Ytt_abs2*Vm[busIdx[lines[l].to]]^2
                    + 2*Vm[busIdx[lines[l].from]]*Vm[busIdx[lines[l].to]]*(Yre*cos(Va[busIdx[lines[l].from]]-Va[busIdx[lines[l].to]])-Yim*sin(Va[busIdx[lines[l].from]]-Va[busIdx[lines[l].to]]))
                    )
                    - flowmax <=0)
            end
        end

        debug && @printf("Contingency %d -> Buses: %d  Lines: %d  Generators: %d\n", c, nbus, nline, ngen)
        debug && println("     lines with limits:  ", nlinelim)
    end
    return opfmodel, nothing
end



function mpopf_init_x(opfmodel,opfdata,demdata)
    T = demdata.tsize
    ngen = length(opfdata.generators)
    nbus = length(opfdata.buses)
    for block_id in getLocalBlocksIds(opfmodel)
        mm = opfmodel
        if  block_id> 0
            mm = getchildren(opfmodel)[block_id]
            _Vm=zeros(nbus)   
            for (i,b) in enumerate(opfdata.buses)
                _Vm[i]=0.5*(b.Vmax+b.Vmin); 
            end  
            _Va = opfdata.buses[opfdata.bus_ref].Va * ones(nbus)
            setvalue(getvariable(mm, :Vm), _Vm)
            setvalue(getvariable(mm, :Va), _Va)
            setvalue(getvariable(mm, :Pramp), zeros(ngen))
            setvalue(getvariable(mm, :Qramp), zeros(ngen))
            _Pg=zeros(ngen); 
            _Qg=zeros(ngen); 
            for (i,g) in enumerate(opfdata.generators)
                _Pg[i]=0.5*(g.Pmax+g.Pmin)
                _Qg[i]=0.5*(g.Qmax+g.Qmin)
            end 
            setvalue(getvariable(mm, :Pg), _Pg)
            setvalue(getvariable(mm, :Qg), _Qg)
        else    
            _Pg0=zeros(T,ngen); 
            _Qg0=zeros(T,ngen); 
            for (i,g) in enumerate(opfdata.generators), t in 1:T
                _Pg0[t,i]=0.5*(g.Pmax+g.Pmin)
                _Qg0[t,i]=0.5*(g.Qmax+g.Qmin)
            end 
            setvalue(getvariable(opfmodel, :Pg0), _Pg0)  
  #          setvalue(getvariable(opfmodel, :Qg0), _Qg0)  
        end
            
    end
end

function get_first_stage_solution(opfmodel)
    getvalue(getvariable(opfmodel, :Pg0))
end

function get_load(demdata)
    demdata.Pd
end

function display_solution(opfmodel,opfdata,demdata)
    T = demdata.tsize
    ngen = length(opfdata.generators)
    Pg = getvalue(getvariable(opfmodel, :Pg0))
    rampingcapa=zeros(ngen,T-1)
    for i in 1:ngen, t in 1:T-1
        rampingcapa[i,t] = opfdata.generators[i].ramp_agc - 100*(Pg[t+1,i] - Pg[t,i])
    end
    @show rampingcapa
end
