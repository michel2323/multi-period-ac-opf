#!/bin/bash
#COBALT -t 30
#COBALT -n 24
#COBALT --attrs mcdram=cache:numa=quad
#COBALT -A ExaGrid
echo "Starting Cobalt job script"
export n_nodes=$COBALT_JOBSIZE
export n_mpi_ranks_per_node=64
export n_mpi_ranks=$(($n_nodes * $n_mpi_ranks_per_node))
export n_openmp_threads_per_rank=1
export n_hyperthreads_per_core=1
export n_hyperthreads_skipped_between_ranks=4
export julia_exec=/home/mschanen/src/julia-ae26b25d43/bin/julia
#export julia_model=/home/mschanen/git/structjump-examples/src/scopf_main_1.jl
export julia_model=/home/mschanen/git/multi-period-ac-opf/deterministic/driver.jl
export julia_sysimg=/home/mschanen/src/julia-ae26b25d43/lib/julia/sys.so
export LD_LIBRARY_PATH=/home/mschanen/src/julia-ae26b25d43/lib/julia:$LD_LIBRARY_PATH
echo "n_nodes: $n_nodes"
echo "n_mpi_ranks: $n_mpi_ranks"
echo "LD_LIBRARY_PATH: $LD_LIBRARY_PATH"
#aprun -n $n_mpi_ranks -N $n_mpi_ranks_per_node \
    #--env OMP_NUM_THREADS=$n_openmp_threads_per_rank -cc depth \
      #-d $n_hyperthreads_skipped_between_ranks \
        #-j $n_hyperthreads_per_core \
          #$julia_exec $julia_model PipsNlp false `cat /home/mschanen/git/structjump-examples/src/problems/case_pegase_64`
#aprun -n $n_mpi_ranks -N $n_mpi_ranks_per_node \
    #--env OMP_NUM_THREADS=$n_openmp_threads_per_rank \
          #$julia_exec -J $julia_sysimg $julia_model PipsNlp false `cat /home/mschanen/git/structjump-examples/src/problems/case_pegase_2048`
aprun -n $n_mpi_ranks -N $n_mpi_ranks_per_node \
    --env OMP_NUM_THREADS=$n_openmp_threads_per_rank \
          $julia_exec -J $julia_sysimg $julia_model 
#PipsNlp /home/mschanen/git/multi-period-ac-opf/data/case118 /home/mschanen/git/multi-period-ac-opf/data/case118/sc1 24 63
