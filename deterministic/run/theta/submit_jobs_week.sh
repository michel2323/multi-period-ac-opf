#!/bin/bash

folder=scaling_week

ppn=$1
tp=$2
ctgs=$3
t=$4
nodes=$5

_ctgs=$(($ctgs - 1 ))
mkdir -p $folder/$ppn
cp job_weak_scaling.sh $folder/$ppn/job_template.sh
cd $folder/$ppn
sed -i "s/PPN/$ppn/g" job_template.sh 
sed -i "s/TIME/$t/g" job_template.sh 
sed -i "s/CTGS/$_ctgs/g" job_template.sh 
sed -i "s/TP/$tp/g" job_template.sh 

#factor=$(bc <<< "1536/(24*$ppn)")
#j=$(bc <<< "$i*$factor")
#j=48
sed "s/NODES/$nodes"/g job_template.sh > job.sh
chmod 755 job.sh
if [ "$nodes" -le 10 ]; then
  qsub -q debug-cache-quad job.sh
else
  qsub job.sh
fi
cd ../..

echo "Submitting on $nodes nodes with $ppn processes per node"



