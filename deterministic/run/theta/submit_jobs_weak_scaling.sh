#!/bin/bash

folder=weak_scaling

ppn=$1
tp=$2
ctgs=$3
t=$4

_ctgs=$(($ctgs - 1 ))
mkdir -p $folder/$ppn
cp job_weak_scaling.sh $folder/$ppn/job_template.sh
cd $folder/$ppn
sed -i "s/PPN/$ppn/g" job_template.sh 
sed -i "s/TIME/$t/g" job_template.sh 
sed -i "s/CTGS/$_ctgs/g" job_template.sh 
sed -i "s/TP/$tp/g" job_template.sh 

j=$(bc <<< "($ctgs/32)*(64/$ppn)")
sed "s/NODES/$j"/g job_template.sh > job.sh
chmod 755 job.sh
if [ "$j" -le 10 ]; then
  qsub -q debug-cache-quad job.sh
else
  qsub job.sh
fi
cd ../..

echo "Submitting on $j nodes with $ppn processes per node, $(($ctgs - 1)) contingencies, $tp time periods and $(($tp * $ctgs)) scenarios"



