include("scopf_hybrid.jl")
include("opfdata.jl")

SJ =  StructJuMPSolverInterface



function main()
  try ARGS
  catch(UndefVarError)
    ARGS = UTF8String["data/case118","data/case118/sc1", "5", "10"]
  end  
  if length(ARGS) != 5
    println("Usage: julia mscopf_main.jl case scenario nb_periods nb_contingencies ")
    return
  end
  @show solver = ARGS[1]; shift!(ARGS)
  @show case_path = ARGS[1]; shift!(ARGS)
  @show scenario_path = ARGS[1]; shift!(ARGS)
  @show nb_time_period = parse(Int,ARGS[1]); shift!(ARGS)
  @show nb_ctgs =  parse(Int,ARGS[1]);

  _main(case_path, scenario_path,nb_time_period,nb_ctgs, solver)
end

# 
function prep_data(opfdata)
  for i in 1:length(opfdata.generators)
    opfdata.generators[i].ramp_agc = opfdata.generators[i].Pmax * 0.1
  end  
end  

function _main(case_path, scenario_path, nb_time_period, nb_ctgs, solver)
  rawdata = RawData(case_path, scenario_path)
  network = opf_loaddata(rawdata)
  load = dem_loaddata(rawdata, nb_time_period)
  contingencies = ctgs_loaddata(rawdata, nb_ctgs)

  prep_data(network)

  opfmodel,soldata = scopf_model(rawdata, network, load, contingencies, solver)
  mpopf_init_x(opfmodel,network,load)

  status = scopf_solve(opfmodel, network, solver, false) 
  #status = mpopf_solve(opfmodel, soldata, nb_time_period, nb_ctgs, solver)
  #post_solve(network,soldata)
  #println("----------------------------")
  #@show soldata.Pg0
  #println("----------------------------")
  #@show soldata.Vm
  #println("----------------------------")
  ##@show soldata.Pdelta
  #println("----------------------------")
  #@show soldata.rampup
  #println("----------------------------")
  #return status,opfmodel,load,network,contingencies,soldata
end

main()
