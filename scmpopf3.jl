include("opfdata.jl")

using StructJuMP, JuMP, StructJuMPSolverInterface


type SolutionData
  Pd
  Qd
  Pg0
  Qg0
  Pdelta
  Qdelta
  Vm
  Va
  prices
  cnstr_ref
  rampup  
  function SolutionData(opfdata, opfdem, ramp_cnstr, T , K)
    ngen = length(opfdata.generators)
    nbus = length(opfdata.buses)
    sol = new()
    sol.Pg0 =zeros(ngen) 
    sol.Qg0 =zeros(ngen) 
    sol.Pdelta = Dict([(k,t)=>zeros(ngen) for k in 1:K, t in 1:T])
    sol.Qdelta = Dict([(k,t)=>zeros(ngen) for k in 1:K, t in 1:T])
    sol.Vm = Dict([(k,t)=>zeros(nbus) for k in 1:K, t in 1:T])
    sol.Va = Dict([(k,t)=>zeros(nbus) for k in 1:K, t in 1:T])

    sol.Pd = opfdem.Pd
    sol.Qd = opfdem.Qd

#    sol.cnstr_ref = Dict(:ramp=>ramp_cnstr)
#    sol.prices = Dict()
    sol.rampup  = zeros(ngen,opfdem.tsize)
    sol
  end
end


function mpopf_solve(opfmodel,sol, T, K, solver)   
    status = StructJuMPSolverInterface.sj_solve(opfmodel;solver=solver, with_prof=false)
    sol.Pg0 = getvalue(getvariable(opfmodel,:Pg0))
    sol.Qg0 = getvalue(getvariable(opfmodel,:Pg0))
    for c in getLocalChildrenIds(opfmodel)
      mc = SJ.getModel(opfmodel,c)
      k = block2contingency(c, K)
      t = block2timeperiod(c, K)
      if k > 0
        sol.Pdelta[k,t] = getvalue(getvariable(mc,:Pdelta))
        sol.Qdelta[k,t] = getvalue(getvariable(mc,:Qdelta))
      end
      sol.Vm[k,t] = getvalue(getvariable(mc,:Vm))
      sol.Va[k,t] = getvalue(getvariable(mc,:Va))
    end
  #  sol.prices[:ramp] = getdual(sol.cnstr_ref[:ramp])
  return status
end

function post_solve(opfdata,sol)
  ngen,ntime=size(sol.Pg0)
  for i in 1:ngen, t in 2:ntime
    sol.rampup[i,t] =  opfdata.generators[i].ramp_agc - max(0.0,sol.Pg0[i,t] - sol.Pg0[i,t-1]) 
  end  
end


block2contingency(pid, k) = (pid >= k+1) ? (pid%(k+1)) : pid	
block2timeperiod(pid,k) = convert(Int32,floor((pid-1)/(k+1))) + 1 



function mpopf_model(rawdata, opfdata, opfdem, clist, solver)
    T = opfdem.tsize
    K = length(clist)
    nscen = T*(K+1) # we add a "no contingency event" 


    lines_off = [opfdata.lines[l] for l in clist]


    opfmodel = StructuredModel(num_scenarios=nscen)

    lines = opfdata.lines; buses = opfdata.buses; generators = opfdata.generators; baseMVA = opfdata.baseMVA;
    nbus  = length(buses); nline= length(lines); ngen  = length(generators);
 

#    @variable(opfmodel, generators[i].Pmin <= Pg0[i=1:ngen,t=1:T] <= generators[i].Pmax)
#    @variable(opfmodel, generators[i].Qmin <= Qg0[i=1:ngen,t=1:T] <= generators[i].Qmax)
    @variable(opfmodel, 0 <= Pg0[i=1:ngen,t=1:T] <= generators[i].Pmax)
    @variable(opfmodel, 0 <= Qg0[i=1:ngen,t=1:T] <= generators[i].Qmax)

    @NLobjective(opfmodel, Min,  (1/(nscen+1))*sum{ 
              generators[i].coeff[generators[i].n-2]*(baseMVA*(Pg0[i,t]))^2 +  
              generators[i].coeff[generators[i].n-1]*(baseMVA*(Pg0[i,t])) + 
              generators[i].coeff[generators[i].n  ], i=1:ngen,t=1:T})
 

#    it T>1 
      @NLconstraint(opfmodel, cnstr_ramp[i=1:ngen,t=2:T],  Pg0[i,t] - Pg0[i,t-1] <= opfdata.generators[i].ramp_agc)
#    end  

    for c in getLocalChildrenIds(opfmodel) 
        println("------------------")
        @show k = block2contingency(c, K)
        @show t = block2timeperiod(c, K)
        @show c
        @show K

  
    
    opfdata_c = opfdata
    opfmodel_c = StructuredModel(parent=opfmodel,id=c)
    if k > 0
        @show  clist[k]
        opfdata_c = opf_loaddata(rawdata, lines_off[k]) 
    else
        println("using base case")

    end
    

    #shortcuts for compactness
    lines = opfdata_c.lines; buses = opfdata_c.buses; generators = opfdata_c.generators; baseMVA = opfdata_c.baseMVA;
    busIdx = opfdata_c.BusIdx; FromLines = opfdata_c.FromLines; ToLines = opfdata_c.ToLines; BusGeners = opfdata_c.BusGenerators;
    nbus  = length(buses); nline= length(lines); ngen  = length(generators);
    
    alpha = 0.3
#    @variable(opfmodel_c, generators[i].Pmin<=Pg[i=1:ngen]<=generators[i].Pmax)
#    @variable(opfmodel_c, generators[i].Qmin<=Qg[i=1:ngen]<=generators[i].Qmax)
    @variable(opfmodel_c, 0<=Pg[i=1:ngen]<=generators[i].Pmax)
    @variable(opfmodel_c, 0<=Qg[i=1:ngen]<=generators[i].Qmax)

    if k > 0
      @variable(opfmodel_c, -alpha*generators[i].Pmax<=Pdelta[i=1:ngen]<=alpha*generators[i].Pmax)
      @variable(opfmodel_c, -alpha*generators[i].Qmax<=Qdelta[i=1:ngen]<=alpha*generators[i].Qmax)
      #@NLconstraint(opfmodel_c, _capa1[i=1:ngen], generators[i].Pmin <= Pg[i]+Pdelta[i] <= generators[i].Pmax) 
      #@NLconstraint(opfmodel_c, _capa3[i=1:ngen], generators[i].Qmin <= Qg[i]+Qdelta[i] <= generators[i].Qmax) 
      @NLconstraint(opfmodel_c, _capa1[i=1:ngen], 0 <= Pg[i]+Pdelta[i] <= generators[i].Pmax) 
      @NLconstraint(opfmodel_c, _capa3[i=1:ngen], 0 <= Qg[i]+Qdelta[i] <= generators[i].Qmax) 
    end 

    @variable(opfmodel_c, buses[i].Vmin <= Vm[i=1:nbus] <= buses[i].Vmax)
    @variable(opfmodel_c, Va[1:nbus])

    #fix the voltage angle at the reference bus
#    setlowerbound(Va[opfdata_c.bus_ref], buses[opfdata_c.bus_ref].Va)
#    setupperbound(Va[opfdata_c.bus_ref], buses[opfdata_c.bus_ref].Va)

    if solver == "PipsNlp"
      @NLconstraint(opfmodel_c, _refbus1, Va[opfdata_c.bus_ref] == buses[opfdata_c.bus_ref].Va) 
      @NLconstraint(opfmodel_c, _refbus2, Va[opfdata_c.bus_ref] == buses[opfdata_c.bus_ref].Va) 
    elseif solver == "Ipopt"
      setlowerbound(Va[opfdata_c.bus_ref], buses[opfdata_c.bus_ref].Va)
      setupperbound(Va[opfdata_c.bus_ref], buses[opfdata_c.bus_ref].Va)
    else   
      error("Unsupported solver!")
    end  


    # no objective
    #@NLobjective(opfmodel_c, Min, 0)
    if k > 0
	@NLobjective(opfmodel_c, Min,  (1/(nscen+1))*sum{ 
              generators[i].coeff[generators[i].n-2]*(baseMVA*(Pg[i]+Pdelta[i]))^2 +  
              generators[i].coeff[generators[i].n-1]*(baseMVA*(Pg[i]+Pdelta[i])) + 
              generators[i].coeff[generators[i].n  ], i=1:ngen,t=1:T})
else
	@NLobjective(opfmodel_c, Min,  (1/(nscen+1))*sum{ 
              generators[i].coeff[generators[i].n-2]*(baseMVA*(Pg[i]))^2 +  
              generators[i].coeff[generators[i].n-1]*(baseMVA*(Pg[i])) + 
              generators[i].coeff[generators[i].n  ], i=1:ngen,t=1:T})
end

    # Bind active power duplicates to first stage variables
    @NLconstraint(opfmodel_c, _coupling[i=1:ngen], Pg[i] == Pg0[i,t]) 
    @NLconstraint(opfmodel_c, _coupling[i=1:ngen], Qg[i] == Qg0[i,t]) 


    # power flow balance
        Pd = opfdem.Pd[:,t]
        Qd = opfdem.Qd[:,t]

    #branch admitances
    YffR,YffI,YttR,YttI,YftR,YftI,YtfR,YtfI,YshR,YshI = computeAdmitances(lines, buses, baseMVA)

    for b in 1:nbus
      if k == 0
     @NLconstraint(
        opfmodel_c, 
        ( sum{ YffR[l], l in FromLines[b]} + sum{ YttR[l], l in ToLines[b]} + YshR[b] ) * Vm[b]^2 
        + sum{ Vm[b]*Vm[busIdx[lines[l].to]]  *( YftR[l]*cos(Va[b]-Va[busIdx[lines[l].to]]  ) + YftI[l]*sin(Va[b]-Va[busIdx[lines[l].to]]  )), l in FromLines[b] }  
        + sum{ Vm[b]*Vm[busIdx[lines[l].from]]*( YtfR[l]*cos(Va[b]-Va[busIdx[lines[l].from]]) + YtfI[l]*sin(Va[b]-Va[busIdx[lines[l].from]])), l in ToLines[b]   } 
        - ( sum{baseMVA*(Pg[g]), g in BusGeners[b]} - Pd[b] ) / baseMVA      # Sbus part
        ==0)
     @NLconstraint(
        opfmodel_c,
        ( sum{-YffI[l], l in FromLines[b]} + sum{-YttI[l], l in ToLines[b]} - YshI[b] ) * Vm[b]^2 
        + sum{ Vm[b]*Vm[busIdx[lines[l].to]]  *(-YftI[l]*cos(Va[b]-Va[busIdx[lines[l].to]]  ) + YftR[l]*sin(Va[b]-Va[busIdx[lines[l].to]]  )), l in FromLines[b] }
        + sum{ Vm[b]*Vm[busIdx[lines[l].from]]*(-YtfI[l]*cos(Va[b]-Va[busIdx[lines[l].from]]) + YtfR[l]*sin(Va[b]-Va[busIdx[lines[l].from]])), l in ToLines[b]   }
        - ( sum{baseMVA*(Qg[g]), g in BusGeners[b]} - Qd[b] ) / baseMVA      #Sbus part
        ==0)

     else
     @NLconstraint(
        opfmodel_c, 
        ( sum{ YffR[l], l in FromLines[b]} + sum{ YttR[l], l in ToLines[b]} + YshR[b] ) * Vm[b]^2 
        + sum{ Vm[b]*Vm[busIdx[lines[l].to]]  *( YftR[l]*cos(Va[b]-Va[busIdx[lines[l].to]]  ) + YftI[l]*sin(Va[b]-Va[busIdx[lines[l].to]]  )), l in FromLines[b] }  
        + sum{ Vm[b]*Vm[busIdx[lines[l].from]]*( YtfR[l]*cos(Va[b]-Va[busIdx[lines[l].from]]) + YtfI[l]*sin(Va[b]-Va[busIdx[lines[l].from]])), l in ToLines[b]   } 
        - ( sum{baseMVA*(Pg[g] + Pdelta[g]), g in BusGeners[b]} - Pd[b] ) / baseMVA      # Sbus part
        ==0)
     @NLconstraint(
        opfmodel_c,
        ( sum{-YffI[l], l in FromLines[b]} + sum{-YttI[l], l in ToLines[b]} - YshI[b] ) * Vm[b]^2 
        + sum{ Vm[b]*Vm[busIdx[lines[l].to]]  *(-YftI[l]*cos(Va[b]-Va[busIdx[lines[l].to]]  ) + YftR[l]*sin(Va[b]-Va[busIdx[lines[l].to]]  )), l in FromLines[b] }
        + sum{ Vm[b]*Vm[busIdx[lines[l].from]]*(-YtfI[l]*cos(Va[b]-Va[busIdx[lines[l].from]]) + YtfR[l]*sin(Va[b]-Va[busIdx[lines[l].from]])), l in ToLines[b]   }
        - ( sum{baseMVA*(Qg[g]+Qdelta[g] ), g in BusGeners[b]} - Qd[b] ) / baseMVA      #Sbus part
        ==0)
      end

    end

    # branch/lines flow limits
    nlinelim=0
    for l in 1:nline
      if lines[l].rateA!=0 && lines[l].rateA<1.0e10
        nlinelim += 1
        flowmax=(lines[l].rateA/baseMVA)^2

        #branch apparent power limits (from bus)
        Yff_abs2=YffR[l]^2+YffI[l]^2; Yft_abs2=YftR[l]^2+YftI[l]^2
        Yre=YffR[l]*YftR[l]+YffI[l]*YftI[l]; Yim=-YffR[l]*YftI[l]+YffI[l]*YftR[l]
        @NLconstraint(
          opfmodel_c,
          Vm[busIdx[lines[l].from]]^2 *
          ( Yff_abs2*Vm[busIdx[lines[l].from]]^2 + Yft_abs2*Vm[busIdx[lines[l].to]]^2 
            + 2*Vm[busIdx[lines[l].from]]*Vm[busIdx[lines[l].to]]*(Yre*cos(Va[busIdx[lines[l].from]]-Va[busIdx[lines[l].to]])-Yim*sin(Va[busIdx[lines[l].from]]-Va[busIdx[lines[l].to]])) 
          ) 
          - flowmax <=0)
  
        #branch apparent power limits (to bus)
        Ytf_abs2=YtfR[l]^2+YtfI[l]^2; Ytt_abs2=YttR[l]^2+YttI[l]^2
        Yre=YtfR[l]*YttR[l]+YtfI[l]*YttI[l]; Yim=-YtfR[l]*YttI[l]+YtfI[l]*YttR[l]
        @NLconstraint(
          opfmodel_c, 
          Vm[busIdx[lines[l].to]]^2 *
          ( Ytf_abs2*Vm[busIdx[lines[l].from]]^2 + Ytt_abs2*Vm[busIdx[lines[l].to]]^2
            + 2*Vm[busIdx[lines[l].from]]*Vm[busIdx[lines[l].to]]*(Yre*cos(Va[busIdx[lines[l].from]]-Va[busIdx[lines[l].to]])-Yim*sin(Va[busIdx[lines[l].from]]-Va[busIdx[lines[l].to]]))
          )
          - flowmax <=0)
      end
    end
  end
  #println(opfmodel)
  return opfmodel,SolutionData(opfdata, opfdem, cnstr_ramp, T, K)
end


function mpopf_init_x(opfmodel,opfdata,demdata)
  T = demdata.tsize
  ngen = length(opfdata.generators)
  nbus = length(opfdata.buses)
  for i in getLocalBlocksIds(opfmodel)
    if(i==0)
        _Pg0=zeros(ngen,T); 
        _Qg0=zeros(ngen,T); 
        for (i,g) in enumerate(opfdata.generators), t in 1:T
            _Pg0[i,t]=0.5*(g.Pmax+g.Pmin)
            _Qg0[i,t]=0.5*(g.Qmax+g.Qmin)
        end 
        setvalue(getvariable(opfmodel, :Pg0), _Pg0)  
        setvalue(getvariable(opfmodel, :Qg0), _Qg0)  
    else
        mm = getchildren(opfmodel)[i]
        _Pg=zeros(ngen); 
        _Qg=zeros(ngen); 
        for (i,g) in enumerate(opfdata.generators)
          _Pg[i]=0.5*(g.Pmax+g.Pmin)
          _Qg[i]=0.5*(g.Qmax+g.Qmin)
        end 
        _Vm=zeros(nbus)   
        for (i,b) in enumerate(opfdata.buses)
            _Vm[i]=0.5*(b.Vmax+b.Vmin); 
        end  
        _Va = opfdata.buses[opfdata.bus_ref].Va * ones(nbus)
        setvalue(getvariable(mm, :Vm), _Vm)
        setvalue(getvariable(mm, :Va), _Va)
        setvalue(getvariable(mm, :Pg), _Pg)
        setvalue(getvariable(mm, :Qg), _Qg)
        try
         setvalue(getvariable(mm, :Pdelta), zeros(ngen))
         setvalue(getvariable(mm, :Qdelta), zeros(ngen))
        catch LoadError
          nothing
        end  
    end
  end
end



